package com.shevliuk.markit.util.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public class MarginDecoration extends RecyclerView.ItemDecoration {
    private final static int MARGIN_IN_DIPS = 8;
    private final int mMargin;

    public MarginDecoration(@NonNull Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        mMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, MARGIN_IN_DIPS, metrics);
    }

    public MarginDecoration(@NonNull Context context, int marginDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        mMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, marginDp, metrics);
    }

    @Override
    public void getItemOffsets(@NotNull Rect outRect, @NotNull View view, RecyclerView parent, @NotNull RecyclerView.State state) {
        final int itemPosition = parent.getChildAdapterPosition(view);
        final RecyclerView.Adapter adapter = parent.getAdapter();
        if (itemPosition == RecyclerView.NO_POSITION) {
            return;
        }
        if ((adapter != null) && (itemPosition == adapter.getItemCount() - 1)) {
            outRect.bottom = mMargin;
        }
        outRect.top = mMargin;
    }

}
