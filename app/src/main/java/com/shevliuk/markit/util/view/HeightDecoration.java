package com.shevliuk.markit.util.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public class HeightDecoration extends RecyclerView.ItemDecoration {
    private final static int MARGIN_IN_DIPS = 10;
    private final int mMargin;

    public HeightDecoration(@NonNull Context context) {
        mMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, MARGIN_IN_DIPS, context.getResources().getDisplayMetrics());
    }


    @Override
    public void getItemOffsets(@NotNull Rect outRect, @NotNull View view, @NotNull RecyclerView parent, @NotNull RecyclerView.State state) {
        outRect.right = mMargin;
    }

}
