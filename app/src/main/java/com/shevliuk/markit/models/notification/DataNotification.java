package com.shevliuk.markit.models.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.shevliuk.markit.models.core.Model;

public class DataNotification extends Model {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("push_type")
    @Expose
    private String type;

    @SerializedName("body")
    @Expose
    private String body;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("site_url")
    @Expose
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
