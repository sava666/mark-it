package com.shevliuk.markit.models.core;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseData<T> extends Model implements Parcelable {
    @SerializedName("data")
    @Expose
    private T data;

    @SerializedName("error")
    @Expose
    private Error error;

    @SerializedName("response")
    @Expose
    private Boolean response;

    public BaseData(T data, Error error, Boolean response) {
        this.data = data;
        this.error = error;
        this.response = response;
    }

    protected BaseData(Parcel in) {
        error = in.readParcelable(Error.class.getClassLoader());
        byte tmpResponse = in.readByte();
        response = tmpResponse == 0 ? null : tmpResponse == 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(error, flags);
        dest.writeByte((byte) (response == null ? 0 : response ? 1 : 2));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BaseData> CREATOR = new Creator<BaseData>() {
        @Override
        public BaseData createFromParcel(Parcel in) {
            return new BaseData(in);
        }

        @Override
        public BaseData[] newArray(int size) {
            return new BaseData[size];
        }
    };

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "BaseData{" +
                "data=" + data +
                ", error=" + error +
                ", response=" + response +
                '}';
    }
}
