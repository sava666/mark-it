package com.shevliuk.markit.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import com.shevliuk.markit.AppApplication;
import com.shevliuk.markit.R;
import com.shevliuk.markit.base.error.DefaultHttpErrorHandler;
import com.shevliuk.markit.base.error.EmptyCommand;
import com.shevliuk.markit.base.error.ErrorHandler;
import com.shevliuk.markit.base.error.NetworkErrorHandler;
import com.shevliuk.markit.base.error.UnauthorizedErrorHandler;
import com.shevliuk.markit.base.error.UnknownErrorHandler;
import com.shevliuk.markit.common.Keys;
import com.shevliuk.markit.di.Injectable;
import com.shevliuk.markit.dialog.ErrorDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;


public abstract class BaseFragment<P extends IPresenter, N extends INavigator> extends Fragment
        implements IView, Injectable, HasAndroidInjector, IToolbar, OnCancelListener {

    @Inject
    DispatchingAndroidInjector<Object> fragmentDispatchingAndroidInjector;

    @Inject
    P presenter;
    @Inject
    N navigator;
    @BindView(R.id.toolbar_container)
    @Nullable
    public ViewGroup toolbarContainer;

    private Unbinder unbinder;

    private BaseDialog dialog;

    private boolean fragmentReturnsFromBackStack;

    protected int getToolbarResource() {
        return 0;
    }

    private ErrorDialog errorDialog;

    private void initToolbar() {
        if (toolbarContainer != null && getToolbarResource() != 0)
            updateToolbar(getLayoutInflater().inflate(getToolbarResource(), null));
    }

    private void updateToolbar(View inflate) {
        if (toolbarContainer != null) {
            toolbarContainer.removeAllViews();
            toolbarContainer.addView(inflate);
        }
        createToolbar(inflate);
    }

    @Override
    public void createToolbar(View toolbar) {

    }

    @SuppressLint("HardwareIds")
    public String getDeviceId() {
        return Settings.Secure.getString(requireActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    //TODO create ErrorDialog builder
    private final ErrorHandler unauthorizedErrorHandler = new UnauthorizedErrorHandler(new EmptyCommand() {
        @Override
        public void execute() {
            onAttachDialog();
            errorDialog =  new ErrorDialog(requireActivity(),
                    requireContext().getString(R.string.unauthorized_error));
            errorDialog.show();
        }
    });

    private final ErrorHandler defaultHttpErrorHandler = new DefaultHttpErrorHandler(_arg ->{
        onAttachDialog();
        errorDialog = new ErrorDialog(requireActivity(), _arg);
        errorDialog.show();
    }
    );

    //TODO add retry option for network errors
    private final ErrorHandler networkErrorHandler = new NetworkErrorHandler(_arg -> {
        onAttachDialog();
        errorDialog = new ErrorDialog(requireActivity(), _arg);
        errorDialog.show();
    }
    );

    public void onClickOneMoreTime() {

    }

    private final ErrorHandler unknownErrorHandler = new UnknownErrorHandler(_arg -> {
        onAttachDialog();
        errorDialog = new ErrorDialog(requireActivity(), _arg);
        errorDialog.show();
    });

    {
        unauthorizedErrorHandler.setSuccessor(defaultHttpErrorHandler);
        defaultHttpErrorHandler.setSuccessor(networkErrorHandler);
        networkErrorHandler.setSuccessor(unknownErrorHandler);
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {

    }

    @LayoutRes
    protected abstract int getLayoutResource();

    protected final P requirePresenter() {
        if (presenter == null)
            throw new IllegalStateException("Presenter is not attached!!!");
        return presenter;
    }

    @CallSuper
    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle arguments = getArguments();
        if (arguments != null) parseArguments(arguments);
    }

    protected void parseArguments(final Bundle _arguments) {
    }

    @CallSuper
    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        final View view = inflater.inflate(getLayoutResource(), container, false);
        includeLayout(view);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    protected void includeLayout(final View view) {

    }

    @CallSuper
    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            presenter.attach(this);
        } else {
            presenter.reAttach(this);
        }
        initToolbar();
        onViewReady(savedInstanceState,fragmentReturnsFromBackStack);
        fragmentReturnsFromBackStack = true;
    }

    protected final void hideSoftKeyboard() {
        final InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = requireActivity().getCurrentFocus();
        if (view == null) {
            view = new View(requireActivity());
        }
        if (imm != null){
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected final void showSoftKeyboard(){
        final InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = requireActivity().getCurrentFocus();
        if (imm != null) {
            imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
        }
    }

    protected abstract void onViewReady(final Bundle _savedInstanceState, final Boolean fragmentReturnsFromBackStack);

    protected final ErrorHandler getDefaultErrorHandler() {
        return unauthorizedErrorHandler;
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @CallSuper
    @Override
    public void showError(final Throwable _throwable) {
        unauthorizedErrorHandler.handleException(_throwable);
    }

    @CallSuper
    @Override
    public void onDestroyView() {
        presenter.detach();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
        super.onDestroyView();
    }

    @CallSuper
    @Override
    public void onDestroy() {
        navigator.dispose();
        super.onDestroy();
    }

    @SuppressWarnings("unchecked")
    protected final N getNavigator() {
        return navigator;
    }


    public final <T extends Fragment> void openScreen(final Class<T> _fragmentToShow,
                                                      final @Nullable Bundle _arguments) {
        openScreen(_fragmentToShow, true, _arguments);
    }

    public final <T extends Fragment> void openScreen(final Class<T> _fragmentToShow,
                                                      final boolean _addToBackStack,
                                                      final @Nullable Bundle _arguments) {
      /*  final Fragment currentFragment = getActivity().getSupportFragmentManager()
                .findFragmentById(getActivity().getFragmentContainerId());
        if (currentFragment != null && currentFragment.getClass().equals(_fragmentToShow)) return;*/
        final Fragment fragment;
        if (_arguments == null) {
            fragment = Fragment.instantiate(getActivity(), _fragmentToShow.getName());
        } else {
            fragment = Fragment.instantiate(getActivity(), _fragmentToShow.getName(), _arguments);
        }

        final FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager()
                .beginTransaction();
        if (_addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
        fragmentTransaction.replace(((ViewGroup) getView().getParent()).getId(), fragment, fragment.getClass().getName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    protected void setFullScreen() {
        if (getActivity() != null)
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    protected void cleanFullScreen() {
        if (getActivity() != null)
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void onAttachDialog(){
        if(errorDialog != null && errorDialog.isShowing())
            errorDialog.dismiss();
    }

    protected void setupAnimation(View view){
        Animation fadeOut = new AlphaAnimation(Keys.Animation.ZERO_STAT_ANIMATION, Keys.Animation.ONE_STAT_ANIMATION);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(Keys.Animation.MEDIUM_LONG_ANIMATION);
        fadeOut.setAnimationListener(new Animation.AnimationListener()
        {
            public void onAnimationEnd(Animation animation) {}
            public void onAnimationRepeat(Animation animation) {}
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }
        });
        view.startAnimation(fadeOut);

    }

}