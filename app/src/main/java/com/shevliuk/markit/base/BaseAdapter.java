package com.shevliuk.markit.base;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public abstract class BaseAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    private ArrayList<T> items = new ArrayList<>();
    private ArrayList<T> itemsDuplicate = new ArrayList<>();
    private ItemClick<T> itemClick = item -> {
    };

    public BaseAdapter() {
        setHasStableIds(true);
    }

    public abstract ParameterBuilder<T, ?> initParameters(RecyclerView recyclerView);

    public void add(T object) {
        items.add(object);
        notifyDataSetChanged();
    }

    public void add(int index, T object) {
        items.add(index, object);
        notifyDataSetChanged();
    }

    public void addAll(Collection<? extends T> collection) {
        if (collection != null) {
            items.addAll(collection);
            notifyDataSetChanged();
        }
    }

    public void addAllDuplicate(Collection<? extends T> collection) {
        if (collection != null) {
            itemsDuplicate.clear();
            itemsDuplicate.addAll(collection);
            notifyDataSetChanged();
        }
    }

    public void addAllWithoutClear(Collection<? extends T> collection) {
        if (collection != null) {

            items.clear();
            items.addAll(collection);
            notifyDataSetChanged();
        }
    }

    public void addAll(T... items) {
        addAll(Arrays.asList(items));
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public void remove(String object) {
        items.remove(object);
        notifyDataSetChanged();
    }

    public T getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public List<T> getList() {
        return items;
    }

    public List<T> getListDuplicate() {
        return itemsDuplicate;
    }

    public void updateList(Collection<? extends T> collection) {
        items.clear();
        if (collection != null)
            items.addAll(collection);
        this.notifyDataSetChanged();
    }

    protected ItemClick<T> getItemClick() {
        return itemClick;
    }

    public void setItemClick(ItemClick<T> itemClick) {
        this.itemClick = itemClick;
    }

    public interface ItemClick<T> {
        void onItemClicked(T item);
    }

    public static class ParameterBuilder<T, A extends BaseAdapter<T, ? extends RecyclerView.ViewHolder>> {
        private A adapter;
        private RecyclerView recyclerView;
        private List<T> list;
        private RecyclerView.LayoutManager layoutManager;
        private Boolean nestedScrollingEnabled = false;
        private Boolean hasFixedSize = true;
        private RecyclerView.ItemDecoration decoration;

        public ParameterBuilder(A adapter, RecyclerView recyclerView) {
            this.adapter = adapter;
            this.recyclerView = recyclerView;
        }

        public ParameterBuilder<T, A> addItem(List<T> list) {
            this.list = list;
            return this;
        }

        public ParameterBuilder<T, A> setLayoutManager(RecyclerView.LayoutManager layoutManager) {
            this.layoutManager = layoutManager;
            return this;
        }

        public ParameterBuilder<T, A> setAdapterItemClick(ItemClick<T> itemClick) {
            adapter.setItemClick(itemClick);
            return this;
        }

        public ParameterBuilder<T, A> setNestedScrollingEnabled(Boolean enabled) {
            nestedScrollingEnabled = enabled;
            return this;
        }

        public ParameterBuilder<T, A> setHasFixedSize(Boolean enabled) {
            hasFixedSize = enabled;
            return this;
        }

        public ParameterBuilder<T, A> addItemDecoration(RecyclerView.ItemDecoration decoration) {
            this.decoration = decoration;
            return this;
        }

        public A build() {
            createWithList();
            createWithLayoutManager();
            createWithNestedScrollingEnabled();
            createWithHasFixedSize();
            createWithItemDecoration();
            createWithSetAdapter();
            return adapter;
        }

        private void createWithList() {
            if (list != null) {
                adapter.addAll(list);
            }
        }

        private void createWithLayoutManager() {
            if (layoutManager == null) {
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
            } else {
                recyclerView.setLayoutManager(layoutManager);
            }
        }

        private void createWithNestedScrollingEnabled() {
            recyclerView.setNestedScrollingEnabled(nestedScrollingEnabled);
        }

        private void createWithHasFixedSize() {
            recyclerView.setHasFixedSize(hasFixedSize);
        }

        private void createWithItemDecoration() {
            if (decoration != null) {
                recyclerView.addItemDecoration(decoration);
            }
        }

        private void createWithSetAdapter() {
            recyclerView.setAdapter(adapter);
        }
    }
}