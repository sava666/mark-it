package com.shevliuk.markit.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public interface INavigator {
    boolean navigateBack();
    boolean navigateBack(final boolean _closeAll);
    void dispose();
    <T extends Fragment> void openScreen(final Class<T> _fragmentToShow,
                                         final @Nullable Bundle _arguments);

    <T extends Fragment> void openScreen(final Class<T> _fragmentToShow,
                                         final TypeBackStack _addToBackStack,
                                         final @Nullable Bundle _arguments);

    <T extends Fragment> void openScreen(final Class<T> _fragmentToShow,
                                         final TypeBackStack _addToBackStack,
                                         final @Nullable Bundle _arguments,
                                         boolean isAnimation,
                                         int enterAnimation,
                                         int exitAnimation);

}
