package com.shevliuk.markit.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.CallSuper;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.shevliuk.markit.AppApplication;
import com.shevliuk.markit.R;
import com.shevliuk.markit.base.error.DefaultHttpErrorHandler;
import com.shevliuk.markit.base.error.EmptyCommand;
import com.shevliuk.markit.base.error.ErrorHandler;
import com.shevliuk.markit.base.error.NetworkErrorHandler;
import com.shevliuk.markit.base.error.UnauthorizedErrorHandler;
import com.shevliuk.markit.base.error.UnknownErrorHandler;
import com.shevliuk.markit.di.Injectable;
import com.shevliuk.markit.dialog.ErrorDialog;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;


public abstract class BaseActivity<P extends IPresenter, N extends INavigator> extends AppCompatActivity
        implements IView, Injectable, HasAndroidInjector {

    @Inject
    DispatchingAndroidInjector<Object> fragmentDispatchingAndroidInjector;

    @Inject
    P presenter;
    @Inject
    N navigator;

    private ErrorDialog errorDialog;

    private Unbinder unbinder;

    private BaseDialog dialog;

    private final ErrorHandler unauthorizedErrorHandler = new UnauthorizedErrorHandler(new EmptyCommand() {
        @Override
        public void execute() {
            onAttachDialog();
            errorDialog = new ErrorDialog(BaseActivity.this,
                    AppApplication.getInstance().getResources().getString(R.string.unauthorized_error));
            errorDialog.show();
        }
    });

    private final ErrorHandler defaultHttpErrorHandler = new DefaultHttpErrorHandler(_arg -> {
        onAttachDialog();
        errorDialog = new ErrorDialog(BaseActivity.this, _arg);
        errorDialog.show();
    });

    //TODO add retry option for network errors
    private final ErrorHandler networkErrorHandler = new NetworkErrorHandler(_arg -> {
        onAttachDialog();
        errorDialog = new ErrorDialog(BaseActivity.this, _arg);
        errorDialog.show();
    });

    private final ErrorHandler unknownErrorHandler = new UnknownErrorHandler(_arg -> {
        onAttachDialog();
        errorDialog = new ErrorDialog(BaseActivity.this, _arg);
        errorDialog.show();
    });

    private void onAttachDialog(){
        if(errorDialog!=null && errorDialog.isShowing())
            errorDialog.dismiss();
    }

    {
        unauthorizedErrorHandler.setSuccessor(defaultHttpErrorHandler);
        defaultHttpErrorHandler.setSuccessor(networkErrorHandler);
        networkErrorHandler.setSuccessor(unknownErrorHandler);
    }

    public void onClickOneMoreTime() {

    }

    @LayoutRes
    protected abstract int getLayoutResource();

    //by default i return -1, if the corresponding activity may show a fragment
    //then return it's containers resourceId
    @IdRes
    protected int getFragmentContainerId() {
        return -1;
    }

    protected final P requirePresenter() {
        if (presenter == null)
            throw new IllegalStateException("Presenter is not attached!!!");
        return presenter;
    }

    protected final ErrorHandler getDefaultErrorHandler() {
        return unauthorizedErrorHandler;
    }

    @SuppressWarnings("unchecked")
    protected final N getNavigator() {
        return navigator;
    }

    @SuppressLint("HardwareIds")
    public String getDeviceId() {
        return Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    @SuppressWarnings("unchecked")
    @CallSuper
    @Override
    public void onCreate(@Nullable final Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        if(getLayoutResource()!=0){
            setContentView(getLayoutResource());
        }

        unbinder = ButterKnife.bind(this);
        if (_savedInstanceState == null) {
            presenter.attach(this);
        } else {
            presenter.reAttach(this);
        }
        parseExtras(getIntent());
        onViewReady(_savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.black));
        }
    }


    protected void onViewReady(@Nullable final Bundle _savedInstanceState) {
    }

    protected void parseExtras(@Nullable final Intent _intent) {
    }

    @Nullable
    protected final Fragment getCurrentFragment() {
        if (getFragmentContainerId() == -1) return null;
        return getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    public void showError(final Throwable _throwable) {
        unauthorizedErrorHandler.handleException(_throwable);
    }

    private void unbindViews() {
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @CallSuper
    @Override
    protected void onDestroy() {

        unbindViews();
        presenter.detach();
        navigator.dispose();
        super.onDestroy();
    }

    @CallSuper
    @Override
    public void onBackPressed() {
        final boolean canGoBack = navigator.navigateBack();
        if (!canGoBack) super.onBackPressed();
    }

    protected final void hideSoftKeyboard() {
        final InputMethodManager imm = (InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        if (imm != null){
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected final void showSoftKeyboard(){
        final InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (imm != null) {
            imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
        }
    }
}