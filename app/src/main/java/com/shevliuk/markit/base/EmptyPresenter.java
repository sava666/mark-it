package com.shevliuk.markit.base;

import androidx.annotation.NonNull;

import javax.inject.Inject;

public final class EmptyPresenter extends BasePresenter<EmptyView, EmptyModel> {
    @Inject
    EmptyPresenter(@NonNull final EmptyView _view, @NonNull final EmptyModel _model) {
        super(_view, _model);
    }
}
