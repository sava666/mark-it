package com.shevliuk.markit.base;

import javax.inject.Inject;

public final class EmptyModel extends BaseModel {

    @Inject
    EmptyModel() { }
}