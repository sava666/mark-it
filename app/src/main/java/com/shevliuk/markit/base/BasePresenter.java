package com.shevliuk.markit.base;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;

import com.shevliuk.markit.data.network.error.NetworkException;

import java.lang.ref.WeakReference;

public abstract class BasePresenter<V extends IView, M extends IModel> implements IPresenter<V, M> {

    private WeakReference<V> viewRef;
    private M model;

    public BasePresenter(final @NonNull V _view,
                         final @NonNull M _model) {
        model = _model;
        viewRef = new WeakReference<>(_view);
    }

    @SuppressWarnings("unchecked")
    @CallSuper
    @Override
    public void attach(@NonNull final V _view) {
        viewRef = new WeakReference<>(_view);
        model.setPresenter(this);
    }

    @SuppressWarnings("unchecked")
    @CallSuper
    @Override
    public void reAttach(@NonNull final V _view) {
        viewRef = new WeakReference<>(_view);
        model.setPresenter(this);
    }

    protected final V requireView() {
        if (!isAttached()) {
            throw new IllegalStateException("The view is not attached to the presenter!!!");
        }
        return viewRef.get();
    }

    protected final M requireModel() {
        if (model == null) {
            throw new IllegalStateException("Model is not attached to the presenter!!!");
        }
        return model;
    }

    @CallSuper
    @Override
    public boolean isAttached() {
        return viewRef != null && viewRef.get() != null;
    }

    @CallSuper
    @Override
    public void detach() {
        model.removePresenter();
        viewRef = null;
//        model = null;
    }

    protected void unexpectedError(){
        requireView().showError(NetworkException.unexpectedError(new Throwable()));
    }
}