package com.shevliuk.markit.base.error;


import com.shevliuk.markit.data.network.error.NetworkException;

public final class NetworkErrorHandler extends ErrorHandler<String> {

    public NetworkErrorHandler(final Command<String> _performAction) {
        super(_performAction);
    }

    @SuppressWarnings("unchecked")
    @Override
    public final void handleException(final Throwable _throwable) {
        super.handleException(_throwable);
        final NetworkException erpException = (NetworkException) _throwable;
        if (erpException.getKind() == NetworkException.Kind.NETWORK) {
            performAction.execute(erpException.getMessage());
        } else {
            if (successor != null) successor.handleException(_throwable);
        }
    }
}
