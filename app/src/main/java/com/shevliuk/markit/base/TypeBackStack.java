package com.shevliuk.markit.base;

public enum TypeBackStack {
    ADD_TO_BACK,
    NOT_ADD_TO_BACK,
    REMOVE_LAST_FRAGMENT
}
