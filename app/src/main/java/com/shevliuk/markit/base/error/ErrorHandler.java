package com.shevliuk.markit.base.error;

import androidx.annotation.CallSuper;

import com.shevliuk.markit.data.network.error.NetworkException;


public abstract class ErrorHandler<T> {

    protected ErrorHandler successor;
    protected Command<T> performAction;

    public ErrorHandler(final Command<T> _performAction) {
        performAction = _performAction;
    }

    public final void setSuccessor(final ErrorHandler _successor) {
        successor = _successor;
    }

    @CallSuper
    public void handleException(final Throwable _throwable) {
        if (!(_throwable instanceof NetworkException)) throw new RuntimeException(_throwable);
    }
}