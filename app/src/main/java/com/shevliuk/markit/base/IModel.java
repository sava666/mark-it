package com.shevliuk.markit.base;


import androidx.annotation.NonNull;

public interface IModel<P extends IPresenter> {
    void setPresenter(@NonNull final P _presenter);

    void removePresenter();

    boolean hasPresenter();
}