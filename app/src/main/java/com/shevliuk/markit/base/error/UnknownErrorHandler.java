package com.shevliuk.markit.base.error;


import com.shevliuk.markit.data.network.error.NetworkException;

public final class UnknownErrorHandler extends ErrorHandler<String> {

    public UnknownErrorHandler(final Command<String> _performAction) {
        super(_performAction);
    }

    @Override
    public final void handleException(final Throwable _throwable) {
        super.handleException(_throwable);
        final NetworkException erpException = (NetworkException) _throwable;
        if (erpException.getKind() == NetworkException.Kind.UNEXPECTED) {
            performAction.execute(erpException.getMessage());
        } else {
            if (successor != null) successor.handleException(_throwable);
        }
    }
}