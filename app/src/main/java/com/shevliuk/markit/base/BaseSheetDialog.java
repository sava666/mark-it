package com.shevliuk.markit.base;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseSheetDialog extends BottomSheetDialog {

    private Unbinder unbinder;

    public BaseSheetDialog(@NonNull final Context context) {
        this(context, 0);
    }

    protected BaseSheetDialog(@NonNull final Context context,
                              final int themeResId) {
        super(context, themeResId);
        initDialog();
    }

    @CallSuper
    protected void initDialog() {
        setContentView(getLayoutResource());
        if (getWindow() != null) {
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @LayoutRes
    protected abstract int getLayoutResource();

    @CallSuper
    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        unbinder = ButterKnife.bind(this);
    }

    @CallSuper
    @Override
    public void onDetachedFromWindow() {
        if (unbinder != null) unbinder.unbind();
        super.onDetachedFromWindow();
    }
}