package com.shevliuk.markit.base;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import javax.inject.Inject;

public final class DefaultNavigator implements INavigator {

    private BaseActivity baseActivity;

    @Inject
    DefaultNavigator(final BaseActivity _baseActivity) {
        baseActivity = _baseActivity;
    }

    public final <T extends Activity> void openScreen(final Class<T> _activityToOpen) {
        openScreen(_activityToOpen, false);
    }

    public final <T extends Activity> void openScreen(final Class<T> _activityToOpen,
                                                      final boolean _finishCurrent) {
        openScreen(_activityToOpen, null, null, _finishCurrent, false, false, 0, 0);
    }

    public final <T extends Activity> void openScreen(final Class<T> _activityToOpen,
                                                      final boolean _finishCurrent, int enterAnimation, int exitAnimation) {
        openScreen(_activityToOpen, null, null, _finishCurrent, false, true, enterAnimation, exitAnimation);
    }

    public final <T extends Activity> void openScreen(final Class<T> _activityToOpen,
                                                      final boolean _finishCurrent,
                                                      final boolean _finishAll) {
        openScreen(_activityToOpen, null, null, _finishCurrent, _finishAll, false, 0, 0);
    }

    public final <T extends Activity> void openScreen(final Class<T> _activityToOpen,
                                                      final Pair<String, Bundle> extras,
                                                      final boolean finishCurrent) {
        openScreen(_activityToOpen, extras, null, finishCurrent, false, false, 0, 0);
    }

    public final <T extends Activity> void openScreen(final Class<T> _activityToOpen,
                                                      final Pair<String, Bundle> extras,
                                                      final boolean finishCurrent, int enterAnimation, int exitAnimation) {
        openScreen(_activityToOpen, extras, null, finishCurrent, false, true, enterAnimation, exitAnimation);
    }

    public final <T extends Activity> void openScreen(final Class<T> _activityToOpen,
                                                      final ActivityOptions _activityOptions,
                                                      final boolean finishCurrent) {
        openScreen(_activityToOpen, null, _activityOptions, finishCurrent, false, false, 0, 0);
    }

    public final <T extends Activity> void openScreen(final Class<T> _activityToOpen,
                                                      final Pair<String, Bundle> extras,
                                                      final ActivityOptions _activityOptions,
                                                      final boolean finishCurrent,
                                                      final boolean finishAll, boolean isAnimation, int enterAnimation, int exitAnimation) {
        final Intent launchIntent = new Intent(baseActivity, _activityToOpen);
        if (extras != null) {
            launchIntent.putExtra(extras.first, extras.second);
        }
        ActivityCompat.startActivity(baseActivity, launchIntent,
                _activityOptions != null ? _activityOptions.toBundle() : null);
        if (finishAll)
            baseActivity.finishAffinity();
        else if (finishCurrent) {
            if (isAnimation) {
                baseActivity.overridePendingTransition(enterAnimation, exitAnimation);
            }
            baseActivity.finish();
        }
    }

    public final <T extends Activity> void openScreenForResult(final Class<T> _activityToOpen,
                                                               final Pair<String, Bundle> _extras,
                                                               final ActivityOptions _activityOptions,
                                                               final int _requestCode) {
        final Intent launchIntent = new Intent(baseActivity, _activityToOpen);
        if (_extras != null) {
            launchIntent.putExtra(_extras.first, _extras.second);
        }
        ActivityCompat.startActivityForResult(baseActivity, launchIntent,
                _requestCode, _activityOptions != null ? _activityOptions.toBundle() : null);
    }

    public final void finishWithResult(final int _responseCode,
                                       final Pair<String, Bundle> _extras) {
        final Intent resultIntent = new Intent();
        if (_extras != null) {
            resultIntent.putExtra(_extras.first, _extras.second);
        }
        baseActivity.setResult(_responseCode, resultIntent);
        baseActivity.finish();
    }

    @Override
    public final <T extends Fragment> void openScreen(final Class<T> _fragmentToShow,
                                                      final @Nullable Bundle _arguments) {
        openScreen(_fragmentToShow, TypeBackStack.ADD_TO_BACK, _arguments);
    }

    @Override
    public final <T extends Fragment> void openScreen(final Class<T> _fragmentToShow,
                                                      final TypeBackStack _addToBackStack,
                                                      final @Nullable Bundle _arguments) {
        openScreen(_fragmentToShow, _addToBackStack, _arguments, false, 0, 0);
    }

    @Override
    public final <T extends Fragment> void openScreen(final Class<T> _fragmentToShow,
                                                      final TypeBackStack _addToBackStack,
                                                      final @Nullable Bundle _arguments, boolean isAnimation, int enterAnimation, int exitAnimation) {
        final Fragment currentFragment = baseActivity.getSupportFragmentManager()
                .findFragmentById(baseActivity.getFragmentContainerId());

        if (currentFragment != null && currentFragment.getClass().equals(_fragmentToShow)) {
            return;
        }

        final Fragment fragment;

        fragment = baseActivity.getSupportFragmentManager().getFragmentFactory().instantiate(ClassLoader.getSystemClassLoader(), _fragmentToShow.getName());

        if (_arguments != null) {
            fragment.setArguments(_arguments);
        }

        final FragmentTransaction fragmentTransaction = baseActivity.getSupportFragmentManager().beginTransaction();

        if (isAnimation) {
            fragmentTransaction.setCustomAnimations(enterAnimation, exitAnimation);
        }

        switch (_addToBackStack){
            case ADD_TO_BACK:
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
                break;
            case REMOVE_LAST_FRAGMENT:
                fragmentTransaction.addToBackStack(null);
                baseActivity.getSupportFragmentManager().popBackStack();
                break;
            case NOT_ADD_TO_BACK:
                break;
        }

        //TODO commitALLOWStateLoss cleaning
        fragmentTransaction.replace(baseActivity.getFragmentContainerId(), fragment, fragment.getClass().getName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public final boolean navigateBack() {
        return navigateBack(false);
    }

    @Override
    public final boolean navigateBack(final boolean _closeAll) {
        if (!_closeAll) return baseActivity.getSupportFragmentManager().popBackStackImmediate();
        final FragmentManager fragmentManager = baseActivity.getSupportFragmentManager();
        final int backStackSize = fragmentManager.getBackStackEntryCount();
        for (int i = 0; i < backStackSize; i++) {
            fragmentManager.popBackStack();
        }
        return false;
    }

    @Override
    public final void dispose() {
        baseActivity = null;
    }
}