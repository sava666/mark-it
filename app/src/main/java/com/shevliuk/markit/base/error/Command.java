package com.shevliuk.markit.base.error;

public interface Command<T> {
    void execute(final T arg);
}