package com.shevliuk.markit.base.error;

import com.shevliuk.markit.data.network.error.NetworkException;
import com.shevliuk.markit.models.core.BaseData;

public final class DefaultHttpErrorHandler extends ErrorHandler<String> {

    public DefaultHttpErrorHandler(final Command<String> _performAction) {
        super(_performAction);
    }

    @Override
    public final void handleException(final Throwable _throwable) {
        super.handleException(_throwable);
        final NetworkException erpException = (NetworkException) _throwable;
        BaseData baseData = erpException.getErrorBodyAs(BaseData.class);
        if (erpException.getKind() == NetworkException.Kind.HTTP) {
            performAction.execute(baseData != null ? baseData.getError().getMessage() : erpException.getErrorBodyAs(String.class));
        } else {
            if (successor != null) successor.handleException(_throwable);
        }
    }
}