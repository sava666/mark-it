package com.shevliuk.markit.base;

public interface IView {
    void showError(final Throwable _throwable);
}