package com.shevliuk.markit.di;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.shevliuk.markit.AppApplication;
import com.shevliuk.markit.di.component.DaggerAppComponent;
import com.shevliuk.markit.util.SimpleActivityLifecycleCallbacks;

import dagger.android.AndroidInjection;
import dagger.android.support.AndroidSupportInjection;

public final class InjectionHelper {

    public static void init(final AppApplication _application) {
        DaggerAppComponent.builder()
                .application(_application)
                .build()
                .inject(_application);
        registerActivityInjector(_application);
    }

    private static void registerActivityInjector(final AppApplication _application) {
        _application.registerActivityLifecycleCallbacks(new SimpleActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                super.onActivityCreated(activity, savedInstanceState);
                injectActivity(activity);
               // injectBroadCastReceiver(new NetworkChangeReceiver(), _application);
            }
        });
    }

    private static void injectActivity(final Activity _activity) {
        if (_activity instanceof Injectable) {
            AndroidInjection.inject(_activity);
        }
        if (_activity instanceof FragmentActivity) {
            //TODO probably need to unregister when activity destroyed
            ((FragmentActivity) _activity).getSupportFragmentManager()
                    .registerFragmentLifecycleCallbacks(
                            new FragmentManager.FragmentLifecycleCallbacks() {
                                @Override
                                public void onFragmentCreated(FragmentManager fm, Fragment f,
                                                              Bundle savedInstanceState) {
                                    if (f instanceof Injectable) {
                                        AndroidSupportInjection.inject(f);
                                    }
                                }
                            }, true);
        }
    }

    private static void injectBroadCastReceiver(final BroadcastReceiver broadcastReceiver, final AppApplication _application) {
        AndroidInjection.inject(broadcastReceiver, _application);
    }
}