package com.shevliuk.markit.di.component;

import com.shevliuk.markit.AppApplication;

public interface AuthorizationComponent {
    void inject(final AppApplication app);
}