package com.shevliuk.markit.di.module;

import com.shevliuk.markit.presentation.main.MainActivity;
import com.shevliuk.markit.presentation.main.MainModule;
import com.shevliuk.markit.presentation.main.MainScope;
import com.shevliuk.markit.presentation.splash.SplashActivity;
import com.shevliuk.markit.presentation.splash.SplashModule;
import com.shevliuk.markit.presentation.splash.SplashScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class AuthorizedActivityModule {
    @SplashScope
    @ContributesAndroidInjector(modules = SplashModule.class)
    abstract SplashActivity splashActivity();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity mainActivity();

}