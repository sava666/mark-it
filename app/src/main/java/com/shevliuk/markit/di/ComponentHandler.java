package com.shevliuk.markit.di;

import android.content.Context;

import androidx.annotation.NonNull;

import com.shevliuk.markit.AppApplication;
import com.shevliuk.markit.di.component.AuthorizationComponent;
import com.shevliuk.markit.di.module.AuthorizedComponent;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class ComponentHandler {

    private final AuthorizedComponent.Builder authorizedComponentBuilder;
    private final AppApplication app;

    private AuthorizationComponent authorizationComponent;

    @Inject
    ComponentHandler(@NonNull final AuthorizedComponent.Builder _authorizedComponentBuilder,
                     @NonNull final Context _context) {
        authorizedComponentBuilder = _authorizedComponentBuilder;
        app = (AppApplication) _context.getApplicationContext();
    }

    public final void buildRequiredComponent() {
        authorizationComponent = null;
        authorizationComponent = authorizedComponentBuilder
                .build();
        authorizationComponent.inject(app);
    }
}