package com.shevliuk.markit.di.component;

import android.app.Application;
import com.shevliuk.markit.AppApplication;
import com.shevliuk.markit.data.local.DataStoreModule;
import com.shevliuk.markit.data.network.NetworkModule;
import com.shevliuk.markit.data.notification.fcm.NotificationModule;
import com.shevliuk.markit.di.module.AppModule;
import com.shevliuk.markit.domain.RepositoryModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AndroidSupportInjectionModule.class,
        RepositoryModule.class,
        AppModule.class,
        NetworkModule.class,
        DataStoreModule.class,
        NotificationModule.class
})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(final Application _application);
        AppComponent build();
    }

    void inject(final AppApplication _app);
}