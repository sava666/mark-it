package com.shevliuk.markit.di.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.shevliuk.markit.data.mapper.DataMapper;
import com.shevliuk.markit.data.mapper.GsonDataMapper;

import java.io.File;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module(subcomponents = {
        AuthorizedComponent.class})
public abstract class AppModule {

    @Binds
    @Singleton
    abstract Context application(final Application _app);

    @Provides
    @Singleton
    static SharedPreferences sharedPreferences(final Context _context) {
        return PreferenceManager.getDefaultSharedPreferences(_context);
    }

    @Provides
    @Singleton
    static File provideCacheDirectory(final Context _context) {
        return _context.getCacheDir();
    }

    @Binds
    @Singleton
    abstract DataMapper provideDataMapper(final GsonDataMapper _gsonDataMapper);

}