package com.shevliuk.markit.di.module;

import com.shevliuk.markit.di.scope.AuthorizedScope;
import com.shevliuk.markit.presentation.main.MainContract;
import com.shevliuk.markit.presentation.main.MainModel;
import com.shevliuk.markit.presentation.splash.SplashContract;
import com.shevliuk.markit.presentation.splash.SplashModel;

import dagger.Binds;
import dagger.Module;

@Module(includes = AuthorizedActivityModule.class)
public abstract class AuthorizedModule {

    @AuthorizedScope
    @Binds
    abstract SplashContract.AbsModel provideSplashModel(final SplashModel model);

    @AuthorizedScope
    @Binds
    abstract MainContract.AbsModel provideMainModel(final MainModel model);

}