package com.shevliuk.markit.di.module;



import com.shevliuk.markit.di.component.AuthorizationComponent;
import com.shevliuk.markit.di.scope.AuthorizedScope;

import dagger.Subcomponent;

@AuthorizedScope
@Subcomponent(modules = AuthorizedModule.class)
public interface AuthorizedComponent extends AuthorizationComponent {
    @Subcomponent.Builder
    interface Builder {
        AuthorizedComponent build();
    }
}