package com.shevliuk.markit.data.local.permission;

import android.app.Activity;

import androidx.fragment.app.Fragment;

import com.shevliuk.markit.base.error.Command;

public interface PermissionManager {
    PermissionManager checkPermissionCamera();

    PermissionManager checkPermissionWriteExternalStorage();

    PermissionManager checkPermissionCallPhone();
    <T extends Activity> void checkPermissionResult(T activity);
    <T extends Fragment> void checkPermissionResult(T fragment);
    void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults, Command<PermissionResult> requestPermissionsResult);
}
