package com.shevliuk.markit.data.network.error;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Iterator;

import retrofit2.Response;
import retrofit2.Retrofit;

public final class NetworkException extends RuntimeException {

    public static NetworkException httpError(String url, Response response, Retrofit retrofit) {
        String message = response.code() + " " + response.message();
        return new NetworkException(message, url, response, Kind.HTTP, null, retrofit);
    }

    public static NetworkException networkError(IOException exception) {
        return new NetworkException("Немає підключення до Інтернету", null, null, Kind.NETWORK, exception, null);
    }

    public static NetworkException unexpectedError(Throwable exception) {
        return new NetworkException(exception.getMessage(), null, null, Kind.UNEXPECTED, exception, null);
    }

    /**
     * Identifies the event kind which triggered a {@link NetworkException}.
     */
    public enum Kind {
        /**
         * An {@link IOException} occurred while communicating to the server.
         */
        NETWORK,
        /**
         * A non-200 HTTP status code was received from the server.
         */
        HTTP,
        /**
         * An internal error occurred while attempting to execute a request. It is best practice to
         * re-throw this exception so your application crashes.
         */
        UNEXPECTED
    }

    private final String url;
    private final Response response;
    private final Kind kind;
    private final Retrofit retrofit;

    private NetworkException(final String message,
                             final String url,
                             final Response response,
                             final Kind kind,
                             final Throwable exception,
                             final Retrofit retrofit) {
        super(message, exception);
        this.url = url;
        this.response = response;
        this.kind = kind;
        this.retrofit = retrofit;
    }

    /**
     * The request URL which produced the error.
     */
    public final String getUrl() {
        return url;
    }

    /**
     * Response object containing status code, headers, body, etc.
     */
    public final Response getResponse() {
        return response;
    }

    public final int getResponseCode() {
        return response.code();
    }

    /**
     * The event kind which triggered this error.
     */
    public final Kind getKind() {
        return kind;
    }

    /**
     * The Retrofit this request was executed on
     */
    public final Retrofit getRetrofit() {
        return retrofit;
    }

    /**
     * HTTP response body converted to specified {@code type}. {@code null} if there is no
     * response.
     *
     * @throws IOException if unable to convert the body to the specified {@code type}.
     */
    public final <T> T getErrorBodyAs(final Class<T> type) {
        if (response == null || response.errorBody() == null) {
            return null;
        }
        T t = null;
        if (type.isInstance(response.message())) {
            t = type.cast("Помилка сервера");
            try {
                JSONObject jObj = new JSONObject(response.errorBody().string());
                Iterator<String> keys = jObj.keys();
                if (keys.hasNext()) {
                    JSONArray jsonArray = jObj.getJSONArray(keys.next());
                    StringBuilder message = new StringBuilder();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        message.append(jsonArray.getString(i));
                    }
                    t = type.cast(message.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return t;
        }

        return null;
    }
}