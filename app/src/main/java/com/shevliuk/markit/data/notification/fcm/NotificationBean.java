package com.shevliuk.markit.data.notification.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.shevliuk.markit.R;
import com.shevliuk.markit.common.Keys;
import com.shevliuk.markit.models.notification.DataNotification;
import com.shevliuk.markit.presentation.main.MainActivity;

class NotificationBean {
    static void sendNotification(Context context, DataNotification dataNotification) {

        Intent intent = new Intent(context, MainActivity.class);

        if(dataNotification.getId() != null){
            intent.putExtra(Keys.NotificationData.INTENT_NOTIFICATION_ID, dataNotification.getId());
            intent.putExtra(Keys.NotificationData.INTENT_NOTIFICATION_CATEGORY, dataNotification.getType());
        }

        if(dataNotification.getUrl() != null){
            intent.putExtra(Keys.NotificationData.INTENT_NOTIFICATION_URL, dataNotification.getUrl());
            intent.putExtra(Keys.NotificationData.INTENT_NOTIFICATION_CATEGORY, dataNotification.getType());
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                Keys.NotificationData.INTENT_NOTIFICATION_REQUEST_CODE, intent, PendingIntent.FLAG_ONE_SHOT);


        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(Keys.NotificationData.INTENT_NOTIFICATION_CHANNEL_ID,
                    context.getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH);

            channel.setDescription(Keys.NotificationData.INTENT_NOTIFICATION_DESCRIPTION);
            channel.setShowBadge(true);

            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(channel);
            }
        }
        Uri defaultSoundUri = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, Keys.NotificationData.INTENT_NOTIFICATION_CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(dataNotification.getTitle())
                        .setContentText(dataNotification.getBody())
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        if (mNotificationManager != null) {
            mNotificationManager.notify(Keys.NotificationData.INTENT_NOTIFICATION_NOTIFY_ID, notificationBuilder.build());
        }

    }
}