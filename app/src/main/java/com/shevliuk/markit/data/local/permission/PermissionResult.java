package com.shevliuk.markit.data.local.permission;

public enum PermissionResult {
    PERMISSION_GRANTED,
    PERMISSION_DENIED
}
