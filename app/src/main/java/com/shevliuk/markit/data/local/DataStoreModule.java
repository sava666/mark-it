package com.shevliuk.markit.data.local;



import com.shevliuk.markit.data.local.data.DataStore;
import com.shevliuk.markit.data.local.data.DataStoreImpl;
import com.shevliuk.markit.data.local.permission.PermissionManager;
import com.shevliuk.markit.data.local.permission.PermissionManagerImpl;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class DataStoreModule {

    @Binds
    @Singleton
    abstract DataStore provideDataStore(final DataStoreImpl _dataStore);

    @Binds
    @Singleton
    abstract PermissionManager providePermissionManager(final PermissionManagerImpl _dataStore);


}