package com.shevliuk.markit.data.mapper;

import java.lang.reflect.Type;

public interface DataMapper {
    <T> T fromString(final String _data, final Class<T> _tClass);
    <T> T fromString(final String _data, final Type _tClass);
    <T> String toString(final T _data);
}
