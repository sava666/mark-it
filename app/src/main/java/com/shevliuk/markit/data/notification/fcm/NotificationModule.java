package com.shevliuk.markit.data.notification.fcm;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class NotificationModule {

    @ContributesAndroidInjector
    abstract NotificationFirebaseMessagingServiceImpl notificationFirebaseMessagingService();

}
