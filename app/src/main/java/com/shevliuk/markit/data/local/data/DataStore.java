package com.shevliuk.markit.data.local.data;

public interface DataStore {

    void setCityId(int cityId);

    int getCityId();

}