package com.shevliuk.markit.data.local.data;

import android.content.SharedPreferences;

import com.shevliuk.markit.common.Keys;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class DataStoreImpl implements DataStore {

    private final SharedPreferences sharedPreferences;

    @Inject
    DataStoreImpl(final SharedPreferences _sharedPreferences) {
        sharedPreferences = _sharedPreferences;
    }

    @Override
    public void setCityId(int cityId) {
        sharedPreferences.edit()
                .putInt(Keys.PreferencesDataStore.CITY_ID, cityId)
                .apply();
    }

    @Override
    public int getCityId() {
        return sharedPreferences.getInt(Keys.PreferencesDataStore.CITY_ID, -1);
    }

}