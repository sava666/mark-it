package com.shevliuk.markit.data.local.permission;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.shevliuk.markit.base.error.Command;

import java.util.ArrayList;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class PermissionManagerImpl implements PermissionManager {

    private final int PERMISSION_REQUEST_CODE = 69;
    private ArrayList<String> listPermissionsNeeded = new ArrayList<>();

    @Inject
    PermissionManagerImpl() {

    }

    @Override
    public PermissionManager checkPermissionCamera() {
        addPermissionWillBeChecked(Manifest.permission.CAMERA);
        return this;
    }

    @Override
    public PermissionManager checkPermissionWriteExternalStorage() {
        addPermissionWillBeChecked(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return this;
    }

    @Override
    public PermissionManager checkPermissionCallPhone() {
        addPermissionWillBeChecked(Manifest.permission.CALL_PHONE);
        return this;
    }

    private void addPermissionWillBeChecked(String... permission) {
        listPermissionsNeeded.clear();
        Stream.of(permission).forEach(listPermissionsNeeded::add);
    }

    @Override
    public final <T extends Activity> void checkPermissionResult(T context) {
        onSetPermission(listPermissionsNeeded, context);
    }

    @Override
    public final <T extends Fragment> void checkPermissionResult(T context) {
        onSetPermission(listPermissionsNeeded, context);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults, Command<PermissionResult> requestPermissionsResult) {
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults.length == listPermissionsNeeded.size()) {
            boolean permissionResult = IntStream.of(grantResults).allMatch(value -> value == PackageManager.PERMISSION_GRANTED);
            requestPermissionsResult.execute(permissionResult ? PermissionResult.PERMISSION_GRANTED : PermissionResult.PERMISSION_DENIED);
        }
    }

    private <T extends Activity> void onSetPermission(ArrayList<String> listPermissionsNeeded, T context) {
        if (listPermissionsNeeded == null || listPermissionsNeeded.isEmpty()) {
            return;
        }
        ActivityCompat.requestPermissions(context, listPermissionsNeeded.toArray(new String[0]),
                PERMISSION_REQUEST_CODE);
    }

    private <T extends Fragment> void onSetPermission(ArrayList<String> listPermissionsNeeded, T context) {
        if (listPermissionsNeeded == null || listPermissionsNeeded.isEmpty()) {
            return;
        }
        context.requestPermissions(listPermissionsNeeded.toArray(new String[0]),
                PERMISSION_REQUEST_CODE);
    }
}
