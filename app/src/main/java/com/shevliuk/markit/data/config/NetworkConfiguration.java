package com.shevliuk.markit.data.config;

import com.shevliuk.markit.BuildConfig;

import java.io.File;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Interceptor;
import okhttp3.Request;

@Singleton
public final class NetworkConfiguration {

    private final int CACHE_SIZE = 10 * 1024 * 1024;

    private final File cacheDirectory;
//    private final DataStore dataStore;
    @Inject
    NetworkConfiguration(
            final File _cacheDirectory) {
        cacheDirectory = _cacheDirectory;
    }

    public File getCacheDirectory() {
        return cacheDirectory;
    }

    public int getCacheSize() {
        return CACHE_SIZE;
    }

    public List<Interceptor> getInterceptors() {
        return Collections.singletonList(getAuthRequestInterceptor());
    }

    private Interceptor getAuthRequestInterceptor() {
        return chain -> {
            Request request = chain.request();

            //apply the below headers only for Applaud requests
            if (request.url().toString().startsWith(BuildConfig.BASE_URL)) {
                request = request.newBuilder()
                        .header("Content-Type", "application/json")
                        .header("Accept", "application/json")
//                        .header(Keys.Constant.HEADER_TOKEN_KEY, dataStore.getToken())
//                        .header(Keys.Constant.HEADER_KEY, Keys.Constant.HEADER_VALUE)
                        .build();
            }
            return chain.proceed(request);
        };
    }
}
