package com.shevliuk.markit.common;



public abstract class Keys {

    public abstract class Args {
        public static final String ID_USER = "ID_USER";
        public static final String SMS_CODE_VERIFY_RESPONSE = "SMS_CODE_VERIFY_RESPONSE";
        public static final String PHONE = "PHONE";
        public static final String SHOP_CATEGORY = "SHOP_CATEGORY";
        public static final String SHOP_ID = "SHOP_ID";
        public static final String GET_QR_DATA_RESPONSE = "GetQrDataResponse";
        public static final String GET_QR_DATA_REQUEST = "GetQrDataRequest";
        public static final String GET_FINAL_BILL = "GET_FINAL_BILL";
        public static final int REQUEST_CAMERA_PERMISSION = 999;
        public static final int START_ACTIVITY_FOR_RESULT = 9966;
        public static final String SEARCH_FRAGMENT_RESPONSE = "SEARCH_FRAGMENT_RESPONSE";
        public static final int SEARCH_FRAGMENT_REQUEST_CODE = 111;
        public static final String PAYMENT_PREPROCCESSING_RESPONSE = "PAYMENT_PREPROCCESSING_RESPONSE";
        public static final String QR_PAY_ID = "QR_PAY_ID";
        public static final String TRANSACTION_DETAIL = "TRANSACTION_DETAIL";
        public static final String CURRENT_PHOTO_PATH = "CURRENT_PHOTO_PATH";
        public static final String LOCK_DETAIL = "LOCK_DETAIL";
    }

    public abstract class Constant {
        public static final String CITY_ID = "city_id";
        public static final String SHOP_ID = "shop_id";
        public static final String CATEGORY_ID = "category_id";
        public static final String PRODUCT_CATEGORY_ID = "product_category_id";
        public static final String STRING_ZERO = "0";
        public static final String STRING_ZERO_AND_ZERO = "00";
        public static final String STRING_NULL = "";
        public static final String STRING_POINT = ".";
        public static final String STRING_COMMA = ",";
        public static final String STRING_SPACE = " ";
        public static final String STRING_ENTER = "\n";
        public static final int INT_ZERO = 0;
        public static final int TIME_DEBOUNCE = 1;
        public static final int LENGTH_FILTER = 15;
        public static final String CARD = "CARD";
        public static final String BONUS = "BONUS";
        public static final String BONUS_CARD = "BONUS_CARD";
        public static final String TOKEN = "token";
        public static final String IMAGE = "image";
        public static final String PHONE_DEFAULT = "7000000000";
    }

    class Extras {

    }

    public abstract class YandexConstant{
        public static final String APIKEY = "apikey";
        public static final String GEOCODE = "geocode";
        public static final String FORMAT = "format";
        public static final String JSON = "json";
        public static final String HOUSE = "house";
        public static final String ENTRANCE = "entrance";
        public static final String STREET = "street";
    }

    public abstract class PreferencesDataStore {
        public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
        public static final String REFRESH_TOKEN = "REFRESH_TOKEN";
        public static final String FCM_TOKEN = "FCM_TOKEN";
        public static final String CITY_ID = "CITY_ID";
        public static final String CITY_NAME = "CITY_NAME";
        public static final String PIN_ENCRYPT = "PIN_ENCRYPT";
        public static final String PIN_IV = "PIN_IV";
        public static final String IS_USE_FINGERPRINT = "IS_USE_FINGERPRINT";
    }

    public abstract class ViewConstants{
        public static final int SMS_SYMBOLS_LIMIT = 4;
        public static final int PHONE_SYMBOLS_LIMIT = 10;
        public static final int PASSWORD_SYMBOL_LIMIT = 8;
    }

    public abstract class SmartRemarketWeb{
        public static final String SMART_REMARKET_WEB = "SMART_REMARKET_WEB";
    }

    public abstract class NetworkConstants {
        public static final int READ_TIMEOUT = 2;
        public static final int WRITE_TIMEOUT = 2;
    }

    public abstract class ShopDetailConstants{
        public static final int LONG_LIMIT_STRING = 100;
        public static final int INDEX_NOT_FOUND = -1;
        public static final int START_INDEX = 0;
        public static final int TAG_LIST_LIMIT = 4;
        public static final int INDEX_WITH_DOT = 1;
        public static final int INDEX_WITH_DOT_AND_SPACE = 2;

        public static final int SHOP_INDICATOR_MINIMUM = 1;
    }

    public abstract class Animation{
        public static final int MEDIUM_LONG_ANIMATION = 400;
        public static final int ZERO_STAT_ANIMATION = 0;
        public static final int ONE_STAT_ANIMATION = 1;
    }

    public abstract class Qr{
        public static final int REQUESTED_CAMERA_ID = 0;
    }

    public abstract class Digits{
        public static final int ZERO = 0;
        public static final int ONE = 1;
        public static final int TWO = 2;
    }

    public abstract class StringDigits{
        public static final String ZERO = "0";
    }

    public abstract class RegexMask{
        public static final String REGEX_ZERO_VALIDATION = "^0";
        public static final String REGEX_VALUE_VALIDATION = "[,\\s]";
        public static final String REGEX_MASK_PHONE = "(\\d)(\\d{3})(\\d{3})(\\d+)";
        public static final String VALUE_MASK_PHONE = "$1 ($2)-$3-$4";
    }

    public abstract class PurchaseAdapter{
        public static final int PURCHASE_ITEM_CASHBACK_VIEW_HOLDER = 0;
        public static final int PURCHASE_ITEM_TOTAL_VIEW_HOLDER = 1;
    }

    public abstract class IntentData{
        public static final String INTENT_PHONE_TEL = "tel:";

        //TODO clear test data, replace with true
        public static final String INTENT_EXTRA_EMAIL = "contact@gmail.com";
        public static final String INTENT_EXTRA_SUBJECT = "Contact us";
        public static final String INTENT_EXTRA_EMAIL_TYPE = "text/html";
        public static final String INTENT_EXTRA_EMAIL_TITLE = "Send mail";
    }

    public abstract class NotificationData{
        public static final String INTENT_NOTIFICATION_ID = "NOTIFICATION_ID";
        public static final String INTENT_NOTIFICATION_URL = "NOTIFICATION_URL";
        public static final String INTENT_NOTIFICATION_CATEGORY = "NOTIFICATION_CATEGORY";
        public static final int INTENT_NOTIFICATION_NOTIFY_ID = 1;
        public static final int INTENT_NOTIFICATION_REQUEST_CODE = 0;
        public static final String INTENT_NOTIFICATION_CHANNEL_ID = "smart_remarket";
        public static final String INTENT_NOTIFICATION_DESCRIPTION = "smart_remarket_controller";
    }

    public abstract class NotificationIdentifier {
        public static final String IDENTIFIER_BANNER = "BANNER";
        public static final String IDENTIFIER_BANK = "BANK";
        public static final String IDENTIFIER_SHOP = "SHOP";
        public static final String IDENTIFIER_SUPPLIER = "SUPPLIER";
    }
}