package com.shevliuk.markit.presentation.main;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.shevliuk.markit.R;
import com.shevliuk.markit.base.BaseActivity;
import com.shevliuk.markit.base.DefaultNavigator;
import io.reactivex.disposables.SerialDisposable;


public class MainActivity extends BaseActivity<MainPresenter, DefaultNavigator> implements
        MainContract.View {

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void onViewReady(@Nullable Bundle _savedInstanceState) {
        super.onViewReady(_savedInstanceState);
      }


    @Override
    protected final int getFragmentContainerId() {
        return R.id.frame_layout;
    }


}
