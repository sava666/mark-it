package com.shevliuk.markit.presentation.main;

import androidx.annotation.NonNull;

import com.shevliuk.markit.base.BaseModel;
import com.shevliuk.markit.base.BasePresenter;
import com.shevliuk.markit.base.IView;


public interface MainContract {

    interface View extends IView {

    }

    abstract class Presenter extends BasePresenter<View, AbsModel> {

        Presenter(@NonNull View view, @NonNull AbsModel model) {
            super(view, model);
        }
        abstract void showError(final Throwable _throwable);
    }

    abstract class AbsModel extends BaseModel<Presenter> {
    }
}
