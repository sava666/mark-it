package com.shevliuk.markit.presentation.splash;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;

import com.shevliuk.markit.base.BaseModel;
import com.shevliuk.markit.base.BasePresenter;
import com.shevliuk.markit.base.IView;

public interface SplashContract {
    interface View<T> extends IView {
        <A extends Activity> void openScreen(final Class<A> _activityToOpen);
        Context getContext();
    }

    abstract class Presenter extends BasePresenter<View, AbsModel> {

        Presenter(@NonNull View _view, @NonNull AbsModel _model) {
            super(_view, _model);
        }

    }

    abstract class AbsModel extends BaseModel<Presenter> {

    }
}
