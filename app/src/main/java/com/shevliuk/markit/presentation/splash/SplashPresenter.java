package com.shevliuk.markit.presentation.splash;

import androidx.annotation.NonNull;

import javax.inject.Inject;

public class SplashPresenter extends SplashContract.Presenter {

    @Inject
    SplashPresenter(@NonNull SplashContract.View _view,
                    @NonNull SplashContract.AbsModel _model) {
        super(_view, _model);
    }

}
