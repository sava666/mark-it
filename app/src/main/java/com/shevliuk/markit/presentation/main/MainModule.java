package com.shevliuk.markit.presentation.main;

import com.shevliuk.markit.base.BaseActivity;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MainModule {

    @MainScope
    @Binds
    abstract MainContract.Presenter providePresenter(final MainPresenter presenter);

    @MainScope
    @Binds
    abstract MainContract.View provideView(final MainActivity activity);

    @MainScope
    @Binds
    abstract BaseActivity provideActivity(final MainActivity activity);

}
