package com.shevliuk.markit.presentation.splash;

import com.shevliuk.markit.base.BaseActivity;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class SplashModule {


    //splash activity
    @SplashScope
    @Binds
    abstract SplashContract.Presenter providePresenter(final SplashPresenter splashPresenter);

    @SplashScope
    @Binds
    abstract SplashContract.View provideView(final SplashActivity splashActivity);

    @SplashScope
    @Binds
    abstract BaseActivity provideActivity(final SplashActivity splashActivity);
}
