package com.shevliuk.markit.presentation.main;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;

import com.annimon.stream.Stream;

import javax.inject.Inject;

public class MainPresenter extends MainContract.Presenter {

    @Inject
    MainPresenter(@NonNull MainContract.View view, @NonNull MainModel model) {
        super(view, model);

    }

    @Override
    void showError(Throwable _throwable) {
    }
}
