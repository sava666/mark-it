package com.shevliuk.markit.presentation.splash;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import com.shevliuk.markit.base.BaseActivity;
import com.shevliuk.markit.base.DefaultNavigator;

public class SplashActivity extends BaseActivity<SplashPresenter, DefaultNavigator> implements SplashContract.View<Activity> {


    @Override
    protected int getLayoutResource() {
        return 0;
    }

    @Override
    public void onCreate(@Nullable Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showError(Throwable _throwable) {

    }

    @Override
    public <A extends Activity> void openScreen(final Class<A> _activityToOpen) {
        getNavigator().openScreen(_activityToOpen, true);
    }
}
