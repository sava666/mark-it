package com.shevliuk.markit;
import android.app.Application;

import com.shevliuk.markit.di.ComponentHandler;
import com.shevliuk.markit.di.InjectionHelper;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

/**
 * Created by roman on 25.05.17.
 */
public class AppApplication extends Application implements HasAndroidInjector/*, HasBroadcastReceiverInjector */ {


    @Inject
    protected DispatchingAndroidInjector<Object> dispatchingAndroidInjector;

    @Inject
    protected ComponentHandler componentHandler;

    private static AppApplication INSTANCE;

    @Override
    public void onCreate() {
        super.onCreate();

        InjectionHelper.init(this);

        INSTANCE = this;
        componentHandler.buildRequiredComponent();

    }


    public static AppApplication getInstance() {
        return INSTANCE;
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return dispatchingAndroidInjector;
    }
}