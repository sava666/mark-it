package com.shevliuk.markit.dialog;

import android.content.Context;
import android.widget.TextView;

import androidx.annotation.NonNull;
import com.shevliuk.markit.R;
import com.shevliuk.markit.base.BaseDialog;

import butterknife.BindView;
import butterknife.OnClick;


public final class ErrorDialog extends BaseDialog {

    @BindView(R.id.message)
    protected TextView message;

    private String description;

    public ErrorDialog(@NonNull final Context context, @NonNull final String description) {
        super(context, R.style.AppCompatAlertDialogLightStyle);
        this.description = description;
    }

    @Override
    protected final void initDialog() {
        super.initDialog();
    }

    @Override
    protected final int getLayoutResource() {
        return R.layout.dialog_error;
    }

    @Override
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        message.setText(description);
    }

    @OnClick(R.id.btn_ok)
    final void onOkClicked() {
        dismiss();
    }
}